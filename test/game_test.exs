defmodule GameTest do
  use ExUnit.Case
  import ExUnit.CaptureIO

  defmodule FakeIO do
    @name __MODULE__

    def start_link() do
      Agent.start_link(fn() -> {[], 0} end, name: @name)
    end

    def gets(_) do
      Agent.get_and_update(@name, fn {moves, counter} ->
        case length(moves) > counter do
          true -> {Enum.at(moves, counter), {moves, counter + 1}}
          false -> {:error, {moves, counter}}
        end
      end)
    end

    def set_moves(moves) do
      Agent.update(@name, fn(_) -> {moves, 0} end)
    end
  end

  setup do
    FakeIO.start_link()
    Application.put_env(:game, :io_module, FakeIO)
  end

  test "it tests the truth" do
    FakeIO.set_moves(~w{4 3 7 6})
    assert capture_io(fn ->
      Game.start_game()
    end) =~ "Game Over"
  end

  test "it handles gracefully bad user input" do
    FakeIO.set_moves(~w{foobar 4 3 7 6})
    assert capture_io(fn ->
      Game.start_game()
    end) =~ "Game Over"
  end

  test "it tells the player who won the game" do
    FakeIO.set_moves(~w{4 3 7 6})
    assert capture_io(fn ->
      Game.start_game()
    end) =~ "Game Over, O won"
  end

  test "it tells the player the game has ended in a tie" do
    FakeIO.set_moves(~w{4 8 1 5 6})
    assert capture_io(fn ->
      Game.start_game()
    end) =~ "Game Over, it's a tie"
  end
end
