defmodule Game do
  alias Game.Server

  def start_game do
    {:ok, pid} = Server.start_link(self())
    Server.get_board(pid) |> print_board()
    Server.play_moves(pid, "X")
    loop(pid)
  end

  defp loop(pid) do
    receive do
      {:get_move, ref} -> 
        IO.write("Enter [0-8]")
        move = read_move()
        send pid, {{:new_move, move, "X"}, ref}
        loop(pid)

      {:new_board, board} ->
        print_board(board)
        loop(pid)

      {:game_over, :winner, player} ->
        IO.write("Game Over, #{player} won")
        :ok

      {:game_over, :tie} ->
        IO.write("Game Over, it's a tie")
        :ok
    end
  end

  defp print_board(b) do
    IO.write("\n--------------\n\n")
    IO.write("  #{Enum.at(b, 0)}   #{Enum.at(b, 1)}   #{Enum.at(b, 2)}\n ===+===+=== \n  #{Enum.at(b, 3)}   #{Enum.at(b, 4)}   #{Enum.at(b, 5)}\n ===+===+=== \n  #{Enum.at(b, 6)}   #{Enum.at(b, 7)}   #{Enum.at(b, 8)}\n")
    IO.write("\n--------------\n\n")
  end

  defp read_move() do
    io_mod = Application.get_env(:game, :io_module) || IO
    io_mod.gets(">")
  end
end
