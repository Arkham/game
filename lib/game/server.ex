defmodule Game.Server do
  use GenServer

  @name __MODULE__

  # API

  def start_link(parent) do
    GenServer.start_link(@name, parent)
  end

  def get_board(pid) do
    GenServer.call(pid, :get_board)
  end

  def play_moves(pid, player) do
    GenServer.cast(pid, {:play_moves, player})
  end

  # Callbacks

  def init(parent) do
    {:ok, %{parent: parent, board: [0, 1, 2, 3, 4, 5, 6, 7, 8]}}
  end

  def handle_call(:get_board, _from, %{board: board} = state) do
    {:reply, board, state}
  end

  def handle_cast({:play_moves, player}, state) do
    new_board = choose_move(player, state)
    send state.parent, {:new_board, new_board}

    cond do
      winner(new_board, player) ->
        send state.parent, {:game_over, :winner, player}
        {:stop, :normal, state}
      full(new_board) ->
        send state.parent, {:game_over, :tie}
        {:stop, :normal, state}
      true ->
        new_player = toggle_player(player)
        GenServer.cast(self(), {:play_moves, new_player})
        {:noreply, %{state | board: new_board}}
    end
  end

  # Internals

  def choose_move(player, state) do
    cond do
      player == "X" -> get_humans_turn(state)
      player == "O" -> get_computers_turn(state)
    end
  end

  def toggle_player("X"), do: "O"
  def toggle_player("O"), do: "X"

  def get_humans_turn(%{board: board} = state) do
    next_move = read_move(state)

    case Integer.parse(next_move) do
      :error ->
        get_humans_turn(state)

      {num, _} -> 
        # if its a free space
        if Enum.at(board, num) != "X" && Enum.at(board, num) != "O" do
          List.update_at(board, num, fn(_) -> "X" end)
        else
          get_humans_turn(state)
        end
    end
  end

  def get_computers_turn(%{board: board}) do
    if Enum.at(board, 4) == 4 do
      List.update_at(board, 4, fn(_) -> "O" end)
    else
      comp_turn = evaluate(board, "O")
      List.update_at(board, comp_turn, fn(_) -> "O" end)
    end
  end

  def evaluate(board, player) do
    {move, _score} = minmax(board, player, player)
    move
  end

  def available_moves(board) do
    board
    |> Enum.with_index(0)
    |> Enum.filter(fn({value, _}) -> value != "X" && value != "O" end)
    |> Enum.map(fn({_value, index}) -> index end)
  end

  def minmax(board, original_player, current_player) do
    scores =
      board
      |> available_moves()
      |> Enum.map(fn move ->
        new_board = List.update_at(board, move, fn _ -> current_player end)
        case game_over(new_board) do
          true -> {move, score(new_board, original_player)}
          false ->
            {_, score} = minmax(new_board, original_player, toggle_player(current_player))
            {move, score}
        end
      end)

    best_score(scores, original_player, current_player)
  end

  def best_score(scores, original_player, current_player) when original_player == current_player do
    scores
    |> Enum.max_by(fn {_, score} -> score end)
  end
  def best_score(scores, _original_player, _current_player) do
    scores
    |> Enum.min_by(fn {_, score} -> score end)
  end

  def score(board, player) do
    cond do
      winner(board, player) -> 1
      winner(board, toggle_player(player)) -> -1
      true -> 0
    end
  end

  @winning_lines [
    # horizontal lines
    [0, 1, 2], [3, 4, 5], [6, 7, 8],
    # vertical lines
    [0, 3, 6], [1, 4, 7], [2, 5, 8],
    # diagonal lines
    [0, 4, 8], [2, 4, 6]
  ]

  def winner(board) do
    find_winner(board) != :not_found
  end

  def winner(board, player) do
    find_winner(board) == player
  end

  def find_winner(board) do
    Enum.reduce_while(@winning_lines, :not_found, fn(indexes, _acc) ->
      result =
        indexes
        |> Enum.map(&(Enum.at(board, &1)))
        |> Enum.uniq

      case length(result) do
        1 -> {:halt, List.first(result)}
        _ -> {:cont, :not_found}
      end
    end)
  end

  def full(board) do
    free_spaces = Enum.any?(board, fn(x) -> x != "X" && x != "O" end)
    !free_spaces
  end

  def game_over(board) do
    winner(board) || full(board)
  end

  def read_move(%{parent: parent}) do
    ref = make_ref()
    send parent, {:get_move, ref}

    receive do
      {{:new_move, move, _player}, ^ref} ->
        move
    end
  end
end
